@echo off

if not defined DKDEVBASE ( goto failed )

:startbuild
pdflatex -synctex=1 -interaction=nonstopmode "%DKDEVBASE%\readme_pdf\ReadMe.tex"

REM FS -- Have to run this twice to make the bookmarks work :/
pdflatex -synctex=1 -interaction=nonstopmode "%DKDEVBASE%\readme_pdf\ReadMe.tex"
goto end

:failed
echo DKDEVBASE not defined!
pause
goto end

:end
